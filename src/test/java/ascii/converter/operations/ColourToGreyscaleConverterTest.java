package ascii.converter.operations;

import static org.junit.Assert.*;

import org.junit.Test;

public class ColourToGreyscaleConverterTest {

	
	private ColourToGreyscaleConverter converter = new ColourToGreyscaleConverter();
	
	@Test
	public void blackIsBlack() {
		assertEquals(0, converter.colourToGreyscale(0));
	}
	
	@Test
	public void whiteIsWhite() {
		assertEquals(255, converter.colourToGreyscale(0xFFFFFF));
	}
	
	@Test
	public void redIsDarkGrey() {
		assertEquals(255 / 3, converter.colourToGreyscale(0xFF0000));
	}
	
	@Test
	public void yellowIsLightGrey() {
		assertEquals(255 / 3 * 2, converter.colourToGreyscale(0xFFFF00));
	}
}
