package ascii.converter.operations;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class ProportionalImageScalerTest {

	private ProportionalImageScaler scaler;
	private final static int MAX_WIDTH = 150;
	private final static int MAX_HEIGHT = 200;
	
	@Before
	public void before() {
		scaler = new ProportionalImageScaler(MAX_WIDTH, MAX_HEIGHT);
	}
	
	@Test
	public void calculateScaleSameSize() {		
		assertEquals(1.0, scaler.calculateScale(MAX_WIDTH, MAX_HEIGHT), 0);
	}
	
	@Test
	public void calculateScaleForProportionallyLarerImage() {		
		assertEquals(0.5, scaler.calculateScale(MAX_WIDTH * 2, MAX_HEIGHT * 2), 0);
	}
	
	@Test
	public void calculateScaleForDoubleHeightImage() {		
		assertEquals(0.5, scaler.calculateScale(MAX_WIDTH * 2, MAX_HEIGHT), 0);
	}
	
	@Test
	public void calculateScaleForDoubleWidthImage() {		
		assertEquals(0.5, scaler.calculateScale(MAX_WIDTH, MAX_HEIGHT * 2), 0);
	}
	
	@Test
	public void calculateScaleForPanoramicImage() {		
		assertEquals(0.2, scaler.calculateScale(MAX_WIDTH * 5, MAX_HEIGHT * 2), 0);
	}
}