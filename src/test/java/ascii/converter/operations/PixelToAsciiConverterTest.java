package ascii.converter.operations;

import static org.junit.Assert.*;
import static ascii.converter.operations.GreyscaleToAsciiConverter.GRADIENT;

import org.junit.Test;

public class PixelToAsciiConverterTest {
	
	private GreyscaleToAsciiConverter converter = new GreyscaleToAsciiConverter();
	
	@Test
	public void minValue() {		
		assertEquals(GRADIENT[0], converter.greyscaleToAscii(0));
	}
	
	@Test
	public void maxValue() {		
		assertEquals(GRADIENT[GRADIENT.length - 1], converter.greyscaleToAscii(255));
	}
}
