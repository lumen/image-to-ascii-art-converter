package ascii.converter.cli;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import ascii.converter.ImageConverter;

/**
 * Reads in an image and returns a result produced by the converter.  
 */
public class FileImageConverterRunner<T> {

	private ImageConverter<T> converter;
	
	public FileImageConverterRunner(ImageConverter<T> converter) {
		this.converter = converter;
	}
	
	public T execute(String fileName) throws IOException {
		BufferedImage image = readImage(fileName);
		return converter.convert(image);
	}
	
	private BufferedImage readImage(String fileName) throws IOException {		
		File file = new File(fileName);
		return ImageIO.read(file);		
	}
}
