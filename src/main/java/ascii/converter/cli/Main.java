package ascii.converter.cli;

import java.io.IOException;

import ascii.converter.ImageConverter;
import ascii.converter.ImageToAsciiArtConverter;

/**
 * Prints ascii art representation of the file who's is specified 
 * as command line argument.
 */
public class Main {
	
	private static final String HELP_MESSAGE = "Image to Ascii Art Converter -"
			+ " expects image file name as an input argument and produces"
			+ " ascii art to standard output";
	
	private final static int WIDTH = 150;
	private final static int HEIGHT = 200;

	public static void main(String[] args) {
		if (args.length == 1) {
			execute(args[0]);
		} else {
			exitWithError(HELP_MESSAGE);
		}
	}
	
	private static void execute(String imageFileName) {
		
		ImageConverter<String> imageConverter = new ImageToAsciiArtConverter(
				WIDTH, HEIGHT, System.lineSeparator());
		
		FileImageConverterRunner<String> runner = 
				new FileImageConverterRunner<String>(imageConverter); 
								
		try {
			final String result = runner.execute(imageFileName);
			produceOutput(result);
		} catch (IOException e) {
			exitWithError(e.getMessage());
		}
	}
	
	private static void exitWithError(String message) {
		System.err.println(message);
		System.exit(1);
	}
	
	private static void produceOutput(String result) {
		System.out.println(result);
	}
}
