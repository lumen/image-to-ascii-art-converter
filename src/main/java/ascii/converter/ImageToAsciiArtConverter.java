package ascii.converter;

import java.awt.image.BufferedImage;

import ascii.converter.operations.ColourToGreyscaleConverter;
import ascii.converter.operations.GreyscaleToAsciiConverter;
import ascii.converter.operations.ProportionalImageScaler;

/**
 * Converts a given image into an ascii art
 */
public class ImageToAsciiArtConverter implements ImageConverter<String> {
		
	private ProportionalImageScaler imageScaler;
	private ColourToGreyscaleConverter greyscaleConverter = new ColourToGreyscaleConverter();
	private GreyscaleToAsciiConverter pixelToAsciiConverter = new GreyscaleToAsciiConverter();
	private String newLine;
		
	/**
	 * @param maxWidth preferred maximum width
	 * @param maxHeight preferred maximum height
	 * @param newLine e.g. <code>System.lineSeparator</code> 
	 * or BR tag in case of HTML 
	 */
	public ImageToAsciiArtConverter(int maxWidth, int maxHeight, String newLine) {
		this.imageScaler = new ProportionalImageScaler(maxWidth, maxHeight);
		this.newLine = newLine;
	}
	
	public String convert(BufferedImage image) {
		BufferedImage resizedImage = imageScaler.scaleImageToPreferredSize(image);
		return convertImageToAscii(resizedImage);
	}
	
	private String convertImageToAscii(BufferedImage image) {
		StringBuilder asciiArt = new StringBuilder();
		for (int y = 0; y < image.getHeight(); y++) {
			for (int x = 0; x < image.getWidth(); x++) {
				int colour = image.getRGB(x, y);
				int greyscale = greyscaleConverter.colourToGreyscale(colour);
				char ascii = pixelToAsciiConverter.greyscaleToAscii(greyscale);								
				asciiArt.append(ascii);
			}
			asciiArt.append(newLine);
		}
		return asciiArt.toString();
	}
}