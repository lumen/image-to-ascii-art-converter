package ascii.converter;

import java.awt.image.BufferedImage;

public interface ImageConverter<T> {
	
	public T convert(BufferedImage image);

}
