package ascii.converter.operations;

/**
 * Simple helper converting RGB representation into greyscale value. 
 * This implementation ignores alpha.
 */
public class ColourToGreyscaleConverter {

	public int colourToGreyscale(int rgb) {
        int r = (rgb >> 16) & 0xFF;
        int g = (rgb >> 8) & 0xFF;
        int b = (rgb & 0xFF);
        return (r + g + b) / 3;
	}
}