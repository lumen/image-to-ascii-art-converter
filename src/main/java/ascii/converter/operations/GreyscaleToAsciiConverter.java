package ascii.converter.operations;

public class GreyscaleToAsciiConverter {

	protected final static char[] GRADIENT = 
				new char[] { '@', '%', '#', '*', '+', '=', ':', '.', ' ' };
	
	/**
	 * @param greyscale expects a greyscale value between 0 and 255 inclusive
	 */
	public char greyscaleToAscii(int greyscale) {
		int index = greyscale * (GRADIENT.length - 1) / 255;
		return GRADIENT[index];
	}
}