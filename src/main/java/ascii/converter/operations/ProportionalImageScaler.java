package ascii.converter.operations;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;

/**
 * Scales a given image keeping the original aspect
 */
public class ProportionalImageScaler {
	
	private final int maxWidth;
	private final int maxHeight;
	
	public ProportionalImageScaler(int maxWidth, int maxHeight) {
		this.maxWidth = maxWidth;
		this.maxHeight = maxHeight;
	}
	
	public BufferedImage scaleImageToPreferredSize(BufferedImage image) {
		double scale = calculateScale(image.getWidth(), image.getHeight());
		return scaleImage(image, scale);
	}	
	
	protected BufferedImage scaleImage(BufferedImage originalImage, double scale) {
		
		int newWidth = (int) (originalImage.getWidth() * scale);
		int newHeight = (int) (originalImage.getHeight() * scale);
        
		Image tempImage = originalImage.getScaledInstance(
        		newWidth, newHeight, Image.SCALE_SMOOTH);
        
		BufferedImage resultImage = new BufferedImage(
        		newWidth, newHeight, originalImage.getType());
        
		Graphics2D g = resultImage.createGraphics();
        g.drawImage(tempImage, 0, 0, null);
        g.dispose();
		
        return resultImage;        
	}
	
	protected double calculateScale(int width, int height) {
		double scaleX = (double) maxWidth / width;
		double scaleY = (double) maxHeight / height;
		return Math.min(scaleX, scaleY);
	} 
}
